public class Student {
    private String npm;     
    private String nama;    
    private String prodi;   

    // constructor
    public Student(String npm, String nama, String prodi) {
        this.npm   = npm;
        this.nama    = nama;
        this.prodi   = prodi;
    }
	
	public void setNPM(String npm){
		this.npm = npm;
	}
	
	public void setNama(String nama){
		this.nama = nama;
	}
	
	public void setProdi(String prodi){
		this.prodi = prodi;
	}
	
	public String getNPM(){
		return this.npm;
	}
	
	public String getNama(){
		return this.nama;
	}
	
	public String getProdi(){
		return this.prodi;
	}

}