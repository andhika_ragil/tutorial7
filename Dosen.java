public class Student {
    private String nip;     
    private String nama;    
    private String prodi;   

    // constructor
    public Student(String nip, String nama, String prodi) {
        this.nip   = nip;
        this.nama    = nama;
        this.prodi   = prodi;
    }
	
	public void setnip(String nip){
		this.nip = nip;
	}
	
	public void setNama(String nama){
		this.nama = nama;
	}
	
	public void setProdi(String prodi){
		this.prodi = prodi;
	}
	
	public String getnip(){
		return this.nip;
	}
	
	public String getNama(){
		return this.nama;
	}
	
	public String getProdi(){
		return this.prodi;
	}

}